package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.model.Project;

import java.util.Collections;
import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Nullable
    private ModelAndView modelAndView;

    @Nullable
    @RequestMapping(value = "/project/add", method = RequestMethod.GET)
    public ModelAndView projectAddPage() {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/edit");
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("project", new Project());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/add", method = RequestMethod.POST)
    public ModelAndView projectAdd(@Nullable @ModelAttribute("project") Project project) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projects");
        projectService.update(project);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/edit/{id}", method = RequestMethod.GET)
    public ModelAndView projectEdit(@Nullable @PathVariable final String id) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/edit");
        modelAndView.addObject("project", projectService.findById(id));
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/edit", method = RequestMethod.POST)
    public ModelAndView projectUpdate(@Nullable @ModelAttribute("project") Project project) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projects");
        projectService.update(project);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/view/{id}", method = RequestMethod.GET)
    public ModelAndView projectView(@Nullable @PathVariable final String id) {
        @Nullable final Project project = projectService.findById(id);
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/view");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ModelAndView projectList() {
        @Nullable final List<Project> projectList = projectService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@Nullable @PathVariable final String id) {
        projectService.remove(id);
        @Nullable final List<Project> projectList = projectService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/removeAll", method = RequestMethod.GET)
    public ModelAndView removeAll() {
        projectService.removeAll();
        @Nullable final List<Project> projectList = Collections.emptyList();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }
}
