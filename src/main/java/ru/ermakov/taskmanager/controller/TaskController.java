package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.model.Project;
import ru.ermakov.taskmanager.model.Task;

import java.util.Collections;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @Nullable
    private ModelAndView modelAndView;

    @Nullable
    private List<Project> projectList;

    @Nullable
    @RequestMapping(value = "/task/add", method = RequestMethod.GET)
    public ModelAndView taskAddPage() {
        modelAndView = new ModelAndView();
        projectList = projectService.findAll();
        modelAndView.setViewName("task/edit");
        modelAndView.addObject("task", new Task());
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/add", method = RequestMethod.POST)
    public ModelAndView taskAdd(@Nullable @ModelAttribute("task") Task task) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/tasks");
        taskService.update(task);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/edit/{id}", method = RequestMethod.GET)
    public ModelAndView taskEdit(@Nullable @PathVariable final String id) {
        modelAndView = new ModelAndView();
        @Nullable final Task task = taskService.findById(id);
        projectList = projectService.findAll();
        modelAndView.setViewName("task/edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/edit", method = RequestMethod.POST)
    public ModelAndView taskUpdate(@Nullable @ModelAttribute("task") Task task) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/tasks");
        taskService.update(task);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/view/{id}", method = RequestMethod.GET)
    public ModelAndView taskView(@Nullable @PathVariable final String id) {
        @Nullable final Task task = taskService.findById(id);
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/view");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ModelAndView taskList() {
        @Nullable final List<Task> taskList = taskService.findAll();
        modelAndView = new ModelAndView();
        projectList = projectService.findAll();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("projectList", projectList);
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@Nullable @PathVariable final String id) {
        taskService.remove(id);
        @Nullable final List<Task> taskList = taskService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/removeAll", method = RequestMethod.GET)
    public ModelAndView removeAll() {
        taskService.removeAll();
        @Nullable final List<Task> taskList = Collections.emptyList();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }
}
