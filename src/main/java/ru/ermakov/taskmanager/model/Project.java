package ru.ermakov.taskmanager.model;

public class Project extends AbstractDeal {

    public Project() {

    }

    @Override
    public String toString() {
        return "ID: " + this.getId() +
                "\nuserId: " + this.getUserId() +
                "\nName: " + this.getName() +
                "\nDescription: " + this.getDescription() +
                "\nDateCreated: "+ this.getDateCreated() +
                "\nDateBegin: " + this.getDateBegin() +
                "\nDateEnd: " + this.getDateEnd() +
                "\nStatus: " + this.getReadinessStatus();
    }
}