package ru.ermakov.taskmanager.model;

public class Task extends AbstractDeal {

    private String projectId;

    public Task() {
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "ID: " + this.getId() +
                "\nuserId: " + this.getUserId() +
                "\nprojectId: " + this.projectId +
                "\nName: " + this.getName() +
                "\nDescription: " + this.getDescription() +
                "\nDateCreated: "+ this.getDateCreated() +
                "\nDateBegin: " + this.getDateBegin() +
                "\nDateEnd: " + this.getDateEnd() +
                "\nStatus: " + this.getReadinessStatus();
    }
}
