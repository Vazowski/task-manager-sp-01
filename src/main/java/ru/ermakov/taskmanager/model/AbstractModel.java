package ru.ermakov.taskmanager.model;

import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class AbstractModel {

    private String id = UUID.randomUUID().toString();

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
