package ru.ermakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermakov.taskmanager.api.repository.ITaskRepository;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.model.Task;

import java.util.Date;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
        Task testTask = new Task();
        testTask.setId("222");
        testTask.setProjectId("111");
        testTask.setName("Test task");
        testTask.setDescription("Description for task project");
        testTask.setDateBegin(new Date());
        testTask.setDateEnd(new Date());
        testTask.setReadinessStatus(ReadinessStatus.PLANNED);
        map.put(testTask.getId(), testTask);
    }

    public void update(@NotNull final Task task) {
        map.put(task.getId(), task);
    }

    @Nullable
    public Task findByName(@NotNull final String name) {
        for (final Task task : map.values()) {
            if (task.getName().equals(name))
                return task;
        }
        return null;
    }
}