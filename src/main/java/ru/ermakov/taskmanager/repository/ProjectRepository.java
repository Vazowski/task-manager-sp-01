package ru.ermakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermakov.taskmanager.api.repository.IProjectRepository;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.model.Project;

import java.util.Date;

@Repository
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        Project testProject = new Project();
        testProject.setId("111");
        testProject.setName("Test project");
        testProject.setDescription("Description for test project");
        testProject.setDateBegin(new Date());
        testProject.setDateEnd(new Date());
        testProject.setReadinessStatus(ReadinessStatus.PLANNED);
        map.put(testProject.getId(), testProject);
    }

    public void update(@NotNull final Project project) {
        map.put(project.getId(), project);
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
        for (final Project project : map.values()) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }
}