package ru.ermakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermakov.taskmanager.api.repository.IUserRepository;
import ru.ermakov.taskmanager.model.User;

@Repository
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
    }

    public void update(@NotNull final User user) {
        map.put(user.getId(), user);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        for (final User user : map.values()) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }
}
