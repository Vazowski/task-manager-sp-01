package ru.ermakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermakov.taskmanager.api.repository.ITaskRepository;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.model.Task;
import ru.ermakov.taskmanager.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    public TaskService() {
    }

    public void update(@Nullable final Task task) {
        if (task == null)
            return;
        taskRepository.update(task);
    }

    @Nullable
    public Task findById(@Nullable final String id) {
        if (id.isEmpty())
            return null;
        return taskRepository.findById(id);
    }

    @Nullable
    public Task findByName(@Nullable final String name) {
        if (name.isEmpty())
            return null;
        return taskRepository.findByName(name);
    }

    @Nullable
    public List<Task> findAll() {
        List<Task> list = taskRepository.findAll();
        if (list.size() > 0)
            return list;
        return null;
    }

    public void remove(@NotNull final String id) {
        if (id.isEmpty())
            return;
        taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.clear();
    }
}