package ru.ermakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermakov.taskmanager.api.repository.IUserRepository;
import ru.ermakov.taskmanager.api.service.IUserService;
import ru.ermakov.taskmanager.model.User;

import java.util.List;

@Service
public class UserService extends AbstractService implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    public UserService() {
    }

    public void update(@Nullable final User user) {
        if (user == null)
            return;
        userRepository.update(user);
    }

    @Nullable
    public User findById(@Nullable final String id) {
        if (id.isEmpty())
            return null;
        return userRepository.findById(id);
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login.isEmpty())
            return null;
        return userRepository.findByLogin(login);
    }

    @Nullable
    public List<User> findAll() {
        List<User> list = userRepository.findAll();
        if (list.size() > 0)
            return list;
        return null;
    }

    public void remove(@NotNull final String id) {
        if (id.isEmpty())
            return;
        userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.clear();
    }
}
