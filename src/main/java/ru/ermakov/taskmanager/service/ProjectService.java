package ru.ermakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermakov.taskmanager.api.repository.IProjectRepository;
import ru.ermakov.taskmanager.api.repository.ITaskRepository;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.model.Project;

import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;
    @Autowired
    private ITaskRepository taskRepository;

    public ProjectService() {
    }

    public void update(@Nullable final Project project) {
        if (project == null)
            return;
        projectRepository.update(project);
    }

    @Nullable
    public Project findById(@Nullable final String id) {
        if (id.isEmpty())
            return null;
        return projectRepository.findById(id);
    }

    @Nullable
    public Project findByName(@Nullable final String name) {
        if (name.isEmpty())
            return null;
        return projectRepository.findByName(name);
    }

    @Nullable
    public List<Project> findAll() {
        List<Project> list = projectRepository.findAll();
        if (list.size() > 0)
            return list;
        return null;
    }

    public void remove(@NotNull final String id) {
        if (id.isEmpty())
            return;
        taskRepository.remove(id);
        projectRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.clear();
        projectRepository.clear();
    }
}
