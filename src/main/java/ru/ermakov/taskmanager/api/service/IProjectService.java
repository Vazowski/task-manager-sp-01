package ru.ermakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    void update(@Nullable final Project project);

    Project findById(@Nullable final String id);

    Project findByName(@Nullable final String name);

    List<Project> findAll();

    void remove(@NotNull final String id);

    void removeAll();
}
