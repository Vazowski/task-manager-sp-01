package ru.ermakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    void update(@Nullable final Task task);

    Task findById(@Nullable final String id);

    Task findByName(@Nullable final String name);

    List<Task> findAll();

    void remove(@NotNull final String id);

    void removeAll();
}