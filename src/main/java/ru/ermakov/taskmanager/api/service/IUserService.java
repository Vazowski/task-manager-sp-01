package ru.ermakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.model.User;

import java.util.List;

public interface IUserService {

    void update(@Nullable final User user);

    User findById(@Nullable final String id);

    User findByLogin(@Nullable final String login);

    List<User> findAll();

    void remove(@NotNull final String id);

    void removeAll();
}
