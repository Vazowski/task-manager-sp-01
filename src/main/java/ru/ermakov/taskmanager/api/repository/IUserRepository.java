package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.model.User;

public interface IUserRepository extends IAbstractRepository<User>{

    void update(@NotNull final User user);

    User findByLogin(@NotNull final String login);
}
