package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.model.Task;

public interface ITaskRepository extends IAbstractRepository<Task>{

    void update(@NotNull final Task task);

    Task findByName(@NotNull final String name);
}
