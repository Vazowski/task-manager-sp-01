package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.model.Project;

public interface IProjectRepository extends IAbstractRepository<Project>{

    void update(@NotNull final Project project);

    Project findByName(@NotNull final String name);
}
