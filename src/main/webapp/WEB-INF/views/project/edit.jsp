<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Project edit</title>
</head>
<body style="padding: 0px 30px">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <span class="navbar-brand mb-0 h1">Task manager</span>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <a class="nav-item nav-link" href="/projects">Project</a>
            <a class="nav-item nav-link" href="/tasks">Task</a>
            <a class="nav-item nav-link" href="#">User</a>
        </ul>
    </div>
</nav>
<c:if test="${empty project.id}">
    <c:url value="/project/add" var="var"/>
</c:if>
<c:if test="${!empty project.id}">
    <c:url value="/project/edit" var="var"/>
</c:if>
<form action="${var}" method="POST" style="padding-left: 50px">
    <c:if test="${!empty project.id}">
        <input type="hidden" name="id" value="${project.id}">
    </c:if>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputName">Name</label>
            <input type="text" class="form-control" id="inputName" name="name" value="${project.name}">
        </div>
        <div class="form-group col-md-2">
            <label for="inputDateBegin">DateBegin</label>
            <form:input path="project.dateBegin" type="date" class="form-control" id="inputDateBegin" name="dateBegin"/>
        </div>
        <div class="form-group col-md-2">
            <label for="inputDateEnd">DateEnd</label>
            <form:input path="project.dateEnd" type="date" class="form-control" id="inputDateEnd" name="dateEnd"/>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputDescription">Description</label>
            <input type="text" class="form-control" id="inputDescription" name="description" value="${project.description}">
        </div>
        <div class="form-group col-md-4">
            <label for="inputStatus">Status</label>
            <select class="form-control" id="inputStatus" name="readinessStatus">
                <c:forEach var="status" items="${statusList}">
                    <c:if test="${project.readinessStatus == status}">
                        <option selected value="${status}">${status.displayName}</option>
                    </c:if>
                    <c:if test="${project.readinessStatus != status}">
                        <option value="${status}">${status.displayName}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
    </div>
    <input class="btn btn-dark" type="submit" value="Done">
</form>
<table>
    <tr>
        <td>
            <form action="/projects" method="get" style="padding-left: 50px">
                <input class="btn btn-dark" type="submit" value="Abort">
            </form>
        </td>
    </tr>
</table>
</body>
</html>